import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url:string;
  public identity;
  public token;
  constructor(private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
   }

  register(user):Observable<any>{
    let json=JSON.stringify(user);
    let params="json="+json;
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');

    return this._httpClient.post(this.url+'register',params,{headers:headers});
  }

  login(user,getToken=null):Observable<any>{
    if(getToken!=null){
      user.getToken='true';
    }
    let json=JSON.stringify(user);
    let params="json="+json;
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');

    return this._httpClient.post(this.url+'login',params,{headers:headers});
  }

  getIdentity(){
    let identity=JSON.parse(localStorage.getItem('identity'));
    if(identity!='undefined'){
      this.identity=identity;
      return this.identity;
    }
  }

  getToken(){
    let token=localStorage.getItem('token');
    if(token!='undefined'){
      this.token=token;
      return this.token;
    }
  }

  update(user,token):Observable<any>{
    let json=JSON.stringify(user);
    let params="json="+json;
    let headers=new HttpHeaders().set('Authorization',token).set('Content-Type','application/x-www-form-urlencoded');

    return this._httpClient.post(this.url+'update',params,{headers:headers});
  }

}
