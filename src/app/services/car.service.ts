import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

//ruta global API
import { GLOBAL } from '../services/global';

@Injectable({
  providedIn: 'root'
})
export class CarService {
  public url: string;
  public token: string;
  constructor(private _httpclient: HttpClient) {
    this.url = GLOBAL.url;
  }

  //todos los coches
  getAllCars(): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
    return this._httpclient.get(this.url + 'cars', { headers: headers })
  }

  //Nuevo coche
  storeCar(token, car): Observable<any> {
    let headers = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/x-www-form-urlencoded');
    let json = JSON.stringify(car);
    let params = "json=" + json;

    return this._httpclient.post(this.url + 'car/new', params, { headers: headers });
  }

  //detalle del coche
  getCar(carId): Observable<any> {
    return this._httpclient.get(this.url + 'car/show/' + carId);
  }

  //actualizar coche
  updateCar(carId, token, car): Observable<any> {
    let json = JSON.stringify(car);
    let params = "json=" + json;
    let headers = new HttpHeaders().set('Authorization', token).set('Content-Type', 'application/x-www-form-urlencoded');

    return this._httpclient.post(this.url+'car/update/'+carId,params,{headers:headers});
  }

  //borrar coche
  deleteCar(carId,token):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token);

    return this._httpclient.get(this.url+'car/delete/'+carId,{headers:headers});
  }


  searchCars(searchString,order=null):Observable<any>{
      return this._httpclient.get(this.url+'search/'+searchString+'/'+order);
    
  }

  orderAll(order):Observable<any>{
    return this._httpclient.get(this.url+'order/'+order);
  }
}
