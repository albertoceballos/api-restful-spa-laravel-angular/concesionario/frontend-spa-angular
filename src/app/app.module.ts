import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//módulo necesario para los formularios
import { FormsModule } from '@angular/forms';
//módulo necesario para las peticiones AJAX al servidor de la API
import { HttpClientModule } from '@angular/common/http';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { CarNewComponent } from './components/car-new/car-new.component';
import { CarEditComponent } from './components/car-edit/car-edit.component';
import { CarDetailComponent } from './components/car-detail/car-detail.component';
import { ProfileComponent } from './components/profile/profile.component';

//guard
import { IdentityGuard } from './services/identity.guard';
import { SearchComponent } from './components/search/search.component';


//Rutas
const appRoutes: Routes = [

  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: RegisterComponent },
  { path: 'inicio', component: HomeComponent, canActivate: [IdentityGuard] },
  { path: 'crear-coche', component: CarNewComponent, canActivate: [IdentityGuard] },
  { path: 'editar-coche/:id', component: CarEditComponent, canActivate: [IdentityGuard] },
  { path: 'coche/:id', component: CarDetailComponent, canActivate: [IdentityGuard] },
  { path: 'editar-perfil', component: ProfileComponent},
  { path: 'search/:searchString', component:SearchComponent},
  { path: 'search/:searchString/:order', component:SearchComponent},
  { path: '**', component: LoginComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    CarNewComponent,
    CarEditComponent,
    CarDetailComponent,
    ProfileComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
