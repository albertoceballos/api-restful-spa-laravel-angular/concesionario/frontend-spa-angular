import { Component, OnInit } from '@angular/core';

//modelos
import {User} from '../../models/User';

//servicios
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[UserService],
})
export class RegisterComponent implements OnInit {
  public pageTitle:string;
  public user:User;
  public status:string;
  public message:string;

  constructor(private _userService:UserService) {
    this.pageTitle="Regístrate";
    this.user=new User('','ROLE_USER','','','','','','');
    
   }

  ngOnInit() {
    
  }

  onSubmit(){
    this._userService.register(this.user).subscribe(
      response=>{
        this.message=response.message;
        this.status=response.status;
      },
      error=>{
        console.log(error);
        this.status="error";
      }
    );
  }

}
