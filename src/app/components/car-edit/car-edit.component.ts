import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
//servicios
import {CarService} from '../../services/car.service';
import {UserService} from '../../services/user.service';

//modelos
import {Car} from '../../models/Car';

@Component({
  selector: 'app-car-edit',
  templateUrl: '../car-new/car-new.component.html',
  styleUrls: ['./car-edit.component.css'],
  providers: [CarService,UserService]
})
export class CarEditComponent implements OnInit {
  public pageTitle;
  public car:Car;
  public message:string;
  public token:string;
  public resStatus:string;
  constructor(private _carService:CarService, private _userService:UserService ,private _router:Router, private _activatedRoute:ActivatedRoute) {
    this.token=this._userService.getToken();
   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        let carId=params['id'];
        this._carService.getCar(carId).subscribe(
          response=>{
            console.log(response);
            if(response.status=='success'){
              this.car=response.car;
              this.pageTitle="Editar "+this.car.title;
              console.log(this.car);
            }else{
              this.message=response.message;
              
            }
          },
          error=>{
            console.log(error);
            this.message="Error al conecatar con el servidor, inténtalo más tarde";
          }
        );
      }
    );
  }

  onSubmit(){
    this._carService.updateCar(this.car.id,this.token,this.car).subscribe(
      response=>{
        console.log(response);
        this.message=response.message;
        this.resStatus=response.status;
        if(response.status=='success'){
          setTimeout(()=>{
            this._router.navigate(['/coche',this.car.id]);
          },1500);
        }
      },
      error=>{
        console.log(error);
        this.resStatus='error';
        this.message='Error al conectar con el servidor';
      }
    );
  }

}
