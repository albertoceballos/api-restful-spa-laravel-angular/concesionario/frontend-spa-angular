import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//servicios
import { UserService } from '../../services/user.service';
import { CarService } from '../../services/car.service';

//modelos
import { User } from '../../models/User';
import { Car } from '../../models/Car';

@Component({
  selector: 'app-car-new',
  templateUrl: './car-new.component.html',
  styleUrls: ['./car-new.component.css'],
  providers: [UserService, CarService],
})
export class CarNewComponent implements OnInit {
  public pageTitle: string;
  public identity;
  public token;
  public car: Car;
  public user: User;
  public message;
  public resStatus;
  constructor(private _userService: UserService,private _carService:CarService, private _router:Router) {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.pageTitle = "Crear nuevo coche";
    this.car = new Car('', this.identity.sub, '', '', '', null, '', '');
  }

  ngOnInit() {
    
  }

  onSubmit() {
    this._carService.storeCar(this.token,this.car).subscribe(
      response=>{
        this.message=response.message;
        this.resStatus=response.status;
        setTimeout(()=>{
          this._router.navigate(['inicio']);
        },1500);
      },
      error=>{
        console.log(error);
        this.message="Error, inténtalo de nuevo más tarde";
        this.resStatus='error';
      }
    );
  }
}
