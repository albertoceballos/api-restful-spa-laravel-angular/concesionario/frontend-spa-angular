import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//modelo
import {User} from '../../models/User';
//servicio
import {UserService} from '../../services/user.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService],
})
export class LoginComponent implements OnInit {
  public pageTitle:string;
  public user:User;
  public status:string;
  public message:string;
  public token;
  public identity;
  constructor(private _userService:UserService,private _router:Router) {
    this.pageTitle="Login";
    this.user=new User('','','','','','','','');
   }

  ngOnInit() {
  
  }

  onSubmit(){
    localStorage.clear();
    console.log(this.user);
    this._userService.login(this.user).subscribe(
      response=>{
        if(response.status=='error'){
          this.status='error';
          this.message=response.message;
        }else{
          this.identity=response;
          localStorage.setItem('identity',JSON.stringify(this.identity));
          this._userService.login(this.user,true).subscribe(
            response=>{
              this.token=response;
              this.status='success';
              this.message='Login correcto, bienvenido '+this.identity.name+' '+this.identity.surname;
              console.log(this.identity);
              console.log(this.token);
              localStorage.setItem('token',this.token);
              setTimeout(()=>{
                this._router.navigate(['inicio']);
              },2500);          
            },
            error=>{
              console.log(error);
            }
          );
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message="Error al conectar con el servidor";
      }
    );
  }


}
