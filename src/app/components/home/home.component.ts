import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import {style,state,animate,trigger,transition} from '@angular/animations';

//servicios
import {CarService} from '../../services/car.service';
import {UserService} from '../../services/user.service';

//modelos
import {Car} from '../../models/Car';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0}),
        animate(1500, style({opacity:1})) 
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(1500, style({opacity:0})) 
      ])
    ])
  ],
  providers:[CarService, UserService]
})
export class HomeComponent implements OnInit {
  public pageTitle:string;
  public cars:Array<Car>;
  public status;
  public message;
  public identity;
  public token;
  public order;
  constructor(private _userService:UserService, private _carService:CarService,private _router:Router) {
    this.pageTitle="Inicio";
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.order='new';
   }


  ngOnInit() {
    this.getCars();
  }

  getCars(){
    this._carService.getAllCars().subscribe(
      response=>{
        //console.log(response);
        if(response.status=='success'){
          if(response.cars.length>0){
            this.cars=response.cars;
            console.log(this.cars);
          }else{
            this.message="No hay coches en la BB.DD";
          }
        }else{
          this.status="error";
          this.message="Error al cargar los datos";
        }
      },
      error=>{
        console.log(error);
        this.status="error";
        this.message="Error al cargar los datos";
      }
    );
  }

  delete(id){
    this._carService.deleteCar(id,this.token).subscribe(
      response=>{
        if(response.status=='success'){
          this.message=response.message;
          this.getCars();
        }
      },
      error=>{
        console.log(error);
      }
    );
  }

  onSubmit(){
    console.log(this.order);
    this._carService.orderAll(this.order).subscribe(
      response=>{
        if(response.status=='success'){
          if(response.cars.length>0){
            this.cars=response.cars;
            console.log(this.cars);
          }else{
            this.message="No hay coches en la BB.DD";
          }
        }else{
          this.status="error";
          this.message="Error al cargar los datos";
        }
      },
      error=>{
        console.log(error);
        this.status="error";
        this.message="Error al cargar los datos";
      }
    );
  }

}
