import { Component, OnInit } from '@angular/core';
import {trigger,transition,style,animate} from '@angular/animations';
//modelos
import {User} from '../../models/User';

//servicios
import {UserService} from '../../services/user.service';
import { identity } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0}),
        animate(1000, style({opacity:1})) 
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(1000, style({opacity:0})) 
      ])
    ])
  ],
  providers:[UserService],
})
export class ProfileComponent implements OnInit {

  public pageTitle="Editar Perfil";
  public user:User;
  public status:string;
  public message:string;
  public identity;
  public token;
  constructor(private _userService:UserService) {
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.user=new User(this.identity.sub,'ROLE_USER',this.identity.name,this.identity.surname,this.identity.email,'','','');
   }

  ngOnInit() {
  }

  onSubmit(){
    this._userService.update(this.user,this.token).subscribe(
      response=>{
        this.status=response.status;
        this.message=response.message;
        this.identity=response.user;
        console.log(this.identity);
        localStorage.setItem('identity','{"sub":"'+this.identity.id+'","email":"'+this.identity.email+'","name":"'+this.identity.name+'","surname":"'+this.identity.surname+'"}');
        setTimeout(()=>{
          this.message=null;
        },4000);
      },
      error=>{
        console.log(error);
        this.status="error";
      }
    );
  }
}
