import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
//servicios
import {CarService} from '../../services/car.service';

//modelos
import {Car} from '../../models/Car';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css'],
  providers: [CarService]
})
export class CarDetailComponent implements OnInit {
  public pageTitle:string;
  public car:Car;
  public message:string;
  constructor(private _carService:CarService, private _router:Router, private _activatedRoute:ActivatedRoute) {

   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        let carId=params['id'];
        this._carService.getCar(carId).subscribe(
          response=>{
            console.log(response);
            if(response.status=='success'){
              this.car=response.car;
              console.log(this.car);
            }else{
              this.message=response.message;
              
            }
          },
          error=>{
            console.log(error);
            this.message="Error al conecatar con el servidor, inténtalo más tarde";
          }
        );
      }
    );
  }

}
