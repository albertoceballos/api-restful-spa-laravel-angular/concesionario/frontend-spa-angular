import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {style,state,animate,trigger,transition} from '@angular/animations';

//servicios
import {CarService} from '../../services/car.service';
import {UserService} from '../../services/user.service';

//modelos
import {Car} from '../../models/Car';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0}),
        animate(1500, style({opacity:1})) 
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(1500, style({opacity:0})) 
      ])
    ])
  ],
  providers:[CarService, UserService]
})
export class SearchComponent implements OnInit {
  public pageTitle:string;
  public cars:Array<Car>;
  public status;
  public message;
  public identity;
  public token;
  public order;
  public searchString;
  constructor(private _userService:UserService, private _carService:CarService, private _activatedRoute:ActivatedRoute,private _router:Router) {
    this.pageTitle="Búsqueda";
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        this.searchString=params['searchString'];
        if(params['order']){
          this.order=params['order'];
        }else{
          this.order='new';
        }
        this._carService.searchCars(this.searchString,this.order).subscribe(
          response=>{
            console.log(response);
            this.cars=response.cars;
            this.status=response.status;
            this.message=response.message;
          },
          error=>{
            console.log(error);
            this.status='error';
          }
        );
      }
    );
  }

  onSubmit(){
    console.log(this.order,this.searchString);
    this._router.navigate(['search',this.searchString,this.order]);
  }
}
