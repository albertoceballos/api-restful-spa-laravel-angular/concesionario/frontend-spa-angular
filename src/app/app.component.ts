import { Component, DoCheck } from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserService],
})
export class AppComponent implements DoCheck {
  public title = 'coches-laravel';
  public identity;
  public token;
  public search:string;
  constructor(private _userService:UserService,private _router:Router){

  }

  ngDoCheck(){
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
  }

  logout(){
    localStorage.clear();
    this.identity=null;
    this.token=null;
    this._router.navigate(['login']);
  }

  onSubmit(){
    console.log(this.search);
    
    if(this.search==undefined || this.search==""){
      this._router.navigate(['inicio']);
    }else{
      this._router.navigate(['search',this.search]);
    }
    
  }
}
