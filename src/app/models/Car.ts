export class Car{
    public id:number;
    public userId:number;
    public title:string;
    public description:string;
    public price:number;
    public status:string;
    public createdAt:any;
    public updatedAt:any;

    constructor(id,userId,title,description,price,status,createdAt,updatedAt){
        this.id=id;
        this.userId=userId;
        this.title=title;
        this.description=description;
        this.price=price;
        this.status=status;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
    }
}