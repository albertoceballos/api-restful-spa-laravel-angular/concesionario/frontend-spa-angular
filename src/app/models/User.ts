export class User{
    public id:number;
    public role:string;
    public name:string;
    public surname:string;
    public email:string;
    public password:string;
    public createdAt:any;
    public updatedAt:any;

    constructor(id,role,name,surname,email,password,createdAt,updatedAt){
        this.id=id;
        this.role=role;
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.password=password;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
    }
}

